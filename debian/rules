#!/usr/bin/make -f

%:
	dh $@ --with python2

override_dh_install:
	dh_install -XREADME -X.example --list-missing

override_dh_installdocs:
	dh_installdocs
	cp slapos/collect/README.txt \
		debian/slapos-node-unofficial/usr/share/doc/slapos-node-unofficial/README.collect.txt

override_dh_compress:
	dh_compress -X.cfg

override_dh_python2:
	dh_python2 -v

# Generate the upstream tarball from upstream Git (adapted from
# http://wiki.debian.org/SandroTosi/Svn_get-orig-source for Git)
PACKAGE := $(shell dpkg-parsechangelog  | \
	sed -ne 's/^Source: \([a-z.]*\).*/\1/p')

SRC_VERSION := $(shell dpkg-parsechangelog | \
	sed -ne 's/^Version: \([^-]\+\)-.*/\1/p')

ifneq (,$(findstring git, $(SRC_VERSION)))
GIT_VERSION := $(shell echo $(SRC_VERSION) | \
	sed -ne 's/.*git\([a-z0-9]\+\)/\1/p')
else
GIT_VERSION := $(shell echo $(SRC_VERSION) | \
	sed -ne 's/\([0-9]\+:\)\?\([0-9.]\+\)/\2/p')
endif

TARBALL = $(PACKAGE)_$(SRC_VERSION).orig.tar.gz

get-orig-source:
	rm -rf get-orig-source ../$(TARBALL)

	git clone http://git.erp5.org/repos/slapos.core.git \
		get-orig-source/

	cd get-orig-source/ && \
		git archive --prefix=$(PACKAGE)-$(SRC_VERSION).orig/ \
			--format=tar $(GIT_VERSION) `/bin/ls | grep -v master` | \
			gzip --no-name --best > ../../$(TARBALL)

	rm -rf get-orig-source
	@echo "'../$(TARBALL)' created."

.PHONY: get-orig-source
